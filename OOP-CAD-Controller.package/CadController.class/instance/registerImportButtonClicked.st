accessing
registerImportButtonClicked
	view whenImportButtonClickedDo: [
		view filenameInput text = '' ifFalse: [
				commandHandler add: (CadLoadCommand new name: view filenameInput text; context: self).
				view writeOutput: '> file imported'. ] . 
	 ]