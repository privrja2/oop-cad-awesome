accessing
registerFinishLineButtonClicked
	| startPoint |
	view whenLineButtonClickedDo: [ 
		view lineButton state ifFalse: [
			(drawnPoints size > 1) ifTrue: [ 
				startPoint := drawnPoints removeFirst.
				commandHandler add: 
					(CadLineCommand new shape: 
						(CadLine fromStartPoint: startPoint points: drawnPoints copy );
					context: self).
				Transcript show: 'Drawn line with starting point: '; show: startPoint asString; show: ' with points: '; show: drawnPoints asString; cr. 
				drawnPoints removeAll.
			]
		]
	]