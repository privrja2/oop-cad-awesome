accessing
drawRectangle: aPosition
	drawnPoints  
		ifEmpty: [ 
			drawnPoints add: aPosition.
		]
		ifNotEmpty: [
			commandHandler add: 
				(CadRectangleCommand new shape: 
					(CadRectangle fromTopLeftCorner: drawnPoints first bottomRightCorner: aPosition);
				context: self).
			Transcript show: 'Drawn rectangle with top left point: '; show: drawnPoints first asString; show: ' with bottom right point: '; show: aPosition asString; cr. 
			view rectangleButton state: false.
			drawnPoints removeAll.
		].