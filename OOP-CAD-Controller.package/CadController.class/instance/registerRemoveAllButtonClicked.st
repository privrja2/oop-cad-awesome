accessing
registerRemoveAllButtonClicked
	view whenRemoveAllButtonClickedDo: [ 
		commandHandler add: (CadClearCommand new context: self)
	 ]