instance creation
open
	view open.
	
	self registerCommandAccepted.
	self registerCanvasLostFocus.
	self registerExportButtonClicked.
	self registerFinishLineButtonClicked.
	self registerImportButtonClicked.
	self registerRedoButtonClicked.
	self registerRoassalViewMouseClicked.
	self registerUndoButtonClicked.
	self registerRemoveAllButtonClicked.