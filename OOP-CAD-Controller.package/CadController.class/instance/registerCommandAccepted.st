accessing
registerCommandAccepted
	view whenCommandAcceptedDo: [ :command |  
		| parseResult |
		view writeOutput: '> ', command .
		parseResult := CadActionParser new parse: command .
		parseResult isAccepted
			ifTrue: [ commandHandler add: (parseResult result context: self) ]
			ifFalse: [ view writeOutput: parseResult errorMessage ]
		].