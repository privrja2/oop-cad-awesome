accessing
registerExportButtonClicked
	view whenExportButtonClickedDo: [
		view filenameInput text = '' ifFalse: [
				commandHandler add: (CadSaveCommand new name: view filenameInput text; context: self).
				view writeOutput: '> file exported'. ] . 
	 ]
