accessing
registerRoassalViewMouseClicked
	view whenView: TRMouseClick do: [ :event |  
		view lineButton state ifTrue: [ self drawLine: (CadAbsolutePoint from: (self toIntegerPoints: event position)).].
		view circleButton state ifTrue: [ self drawCircle: (CadAbsolutePoint from: (self toIntegerPoints: event position) ). ].
		view rectangleButton state ifTrue: [	self drawRectangle: (CadAbsolutePoint from: (self toIntegerPoints: event position)). ].
		view removeOneButton state ifTrue: [ self invokeCommand: (CadRemoveCommand new context: self; point: (CadAbsolutePoint from: (self toIntegerPoints: event position))).
			]
    ].
