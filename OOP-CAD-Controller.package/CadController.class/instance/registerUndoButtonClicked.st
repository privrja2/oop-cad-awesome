accessing
registerUndoButtonClicked
	view whenUndoButtonClickedDo: [ 
		commandHandler undo.
	 ]