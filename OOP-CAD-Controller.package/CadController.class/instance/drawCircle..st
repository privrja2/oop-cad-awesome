accessing
drawCircle: aPosition
	| radius |
	drawnPoints 
		ifEmpty: [ 
			drawnPoints add: aPosition.
		]
		ifNotEmpty: [ 
			radius := (drawnPoints first point distanceTo: aPosition) asInteger.
			commandHandler add: 
				(CadCircleCommand new shape: 
					(CadCircle fromCenterPoint: drawnPoints first radius: radius);
				context: self).
			Transcript show: 'Drawn circle with center point: '; show: drawnPoints first asString; show: ' with radius: '; show: radius asString; cr. 
			view circleButton state: false.
			drawnPoints removeAll.
		].