accessing
registerCanvasLostFocus
	view whenCanvasLostFocusDo: [ 
		drawnPoints removeAll.
	 ]