as yet unclassified
buildShapes
	|shapes|
	shapes := OrderedCollection new.
	1 to: 50 atRandom do: [ :x | shapes add: self buildShape ].
	^shapes.