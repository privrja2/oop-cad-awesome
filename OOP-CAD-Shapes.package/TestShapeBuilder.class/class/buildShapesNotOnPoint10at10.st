as yet unclassified
buildShapesNotOnPoint10at10
	^OrderedCollection with: (CadCircle new centerPoint: (CadAbsolutePoint new point: 80@80) radius: 5)
		with: (CadRectangle new topLeftCorner: (CadAbsolutePoint new point: 50@50) bottomRightCorner: (CadAbsolutePoint new point: 70@60))
		with: (CadLine new startPoint: (CadAbsolutePoint new point: 50@50) points: (OrderedCollection with: (CadAbsolutePoint new point: 60@50) with: (CadAbsolutePoint new point: 80@70))).