as yet unclassified
buildLine
	|points|
	points := OrderedCollection new.
	1 to: 30 atRandom do: [ :x | points add: self buildAbsolutePoint ].
	^CadLine new startPoint: self buildAbsolutePoint points: points.