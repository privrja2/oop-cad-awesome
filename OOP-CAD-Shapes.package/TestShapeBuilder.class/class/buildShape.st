as yet unclassified
buildShape
	|options|
	options := OrderedCollection with: [ self buildCircle ] with: [ self buildLine ] with: [ self buildRectangle ].
	^options atRandom value.