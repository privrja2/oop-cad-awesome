as yet unclassified
buildShapesOnPoint10at10
	^OrderedCollection with: (CadCircle new centerPoint: (CadAbsolutePoint new point: 10@10) radius: 5)
		with: (CadRectangle new topLeftCorner: (CadAbsolutePoint new point: 0@0) bottomRightCorner: (CadAbsolutePoint new point: 20@20))
		with: (CadLine new startPoint: (CadAbsolutePoint new point: 0@10) points: (OrderedCollection with: (CadAbsolutePoint new point: 20@10))).