as yet unclassified
shiftBy: aShiftPoint
	startPoint translateBy: aShiftPoint.
	points do: [ :point | point translateBy: aShiftPoint. ].