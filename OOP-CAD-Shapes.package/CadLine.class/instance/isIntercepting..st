testing
isIntercepting: aPoint
	|lastPoint|
	lastPoint := startPoint point.
	points do: [ :cadPoint |
		(aPoint onLineFrom: lastPoint to: cadPoint point)
			ifTrue: [^true].
		lastPoint := cadPoint point.
		].
	^false.