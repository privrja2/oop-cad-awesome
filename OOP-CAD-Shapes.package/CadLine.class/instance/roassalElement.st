as yet unclassified
roassalElement
	|ellipseShape lastElement group lastPoint|
	ellipseShape := RTEllipse new.
	lastElement := ellipseShape element.
	lastElement translateTo: startPoint.
	lastPoint := startPoint.
	group := RTGroup new.
	points do: [ :point |
			|absPoint|
			absPoint := point asPoint: lastPoint.
			group add: ((RTEdge from: lastElement to: (ellipseShape element translateTo: absPoint; yourself))).
			lastElement := (ellipseShape element translateTo: absPoint; yourself).
			lastPoint := absPoint.
		].
	group addShape: RTMultiLine.
	^group.