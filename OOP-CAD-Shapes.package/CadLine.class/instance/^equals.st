comparing
= aLine
	^ (self class = aLine class) and: [(self startPoint = aLine startPoint) and: [ self points asArray = aLine points asArray ] ]