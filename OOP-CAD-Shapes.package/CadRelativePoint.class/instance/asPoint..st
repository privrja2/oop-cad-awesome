converting
asPoint: aPreviousPoint
	^ (aPreviousPoint x + self point x) @ (aPreviousPoint y + self point y)