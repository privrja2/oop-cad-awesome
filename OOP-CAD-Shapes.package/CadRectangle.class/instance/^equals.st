comparing
= aRectangle
	^ (self class = aRectangle class) and: [
			(self topLeftCorner = aRectangle topLeftCorner)
				and: [ self bottomRightCorner = aRectangle bottomRightCorner ]]