testing
isIntercepting: aPoint
	^(aPoint >= topLeftCorner point) and: (aPoint <= bottomRightCorner point).