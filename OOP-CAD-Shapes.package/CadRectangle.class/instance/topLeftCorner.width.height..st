accessing
topLeftCorner: aPoint width: aWidth height: aHeight
	topLeftCorner := aPoint .
	width := aWidth .
	height := aHeight .
	bottomRightCorner := CadAbsolutePoint new point: (aPoint x + aWidth) @ (aPoint y + aHeight)