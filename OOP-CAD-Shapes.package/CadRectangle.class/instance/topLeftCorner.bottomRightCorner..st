accessing
topLeftCorner: aSourcePoint bottomRightCorner: aDestinationPoint
	| absDestinationPoint |
	absDestinationPoint := CadAbsolutePoint new point: (aDestinationPoint asPoint: aSourcePoint) .
	topLeftCorner := aSourcePoint .
	bottomRightCorner := aDestinationPoint .
	width := absDestinationPoint x - aSourcePoint x .
	height := absDestinationPoint y - aSourcePoint y 