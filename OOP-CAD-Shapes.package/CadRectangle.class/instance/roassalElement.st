comparing
roassalElement
	|rectangle points absBottomRightPoint|
	absBottomRightPoint := bottomRightCorner asPoint: topLeftCorner.
	rectangle := RTPolygon rectangle.
	points := Array with: topLeftCorner with: (topLeftCorner x + width)@(topLeftCorner y)
			with: absBottomRightPoint with: (absBottomRightPoint x - width)@(absBottomRightPoint y).
	rectangle vertices: points.
	^rectangle element.