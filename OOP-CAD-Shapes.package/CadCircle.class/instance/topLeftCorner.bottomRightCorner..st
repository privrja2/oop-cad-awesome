accessing
topLeftCorner: aSourcePoint bottomRightCorner: aDestinationPoint
	| rad absDestinationPoint|
	topLeftCorner := aSourcePoint.
	absDestinationPoint := CadAbsolutePoint new point: (aDestinationPoint asPoint: aSourcePoint).
	bottomRightCorner := aDestinationPoint.
	width := absDestinationPoint x - aSourcePoint x.
	height := absDestinationPoint y - aSourcePoint y.
	rad := width / 2.
	centerPoint := CadAbsolutePoint new
		point: (aSourcePoint x + rad) @ (aSourcePoint y + (height / 2)).
	radius := width = height
		ifTrue: [ rad ]
		ifFalse: [ [ ^ IllegalState signal ] ]