accessing
topLeftCorner: aTopLeftCorner width: aWidth height: aHeight
	| rad |
	topLeftCorner := aTopLeftCorner .
	bottomRightCorner := CadAbsolutePoint new point: (aTopLeftCorner x + aWidth) @ (aTopLeftCorner y + aHeight) .
	width := aWidth .
	height := aHeight .
	rad := aWidth / 2 .
	centerPoint := CadAbsolutePoint new point: (aTopLeftCorner x + rad) @ (aTopLeftCorner y + (aHeight / 2)) .
	aWidth = aHeight 
		ifTrue: [ radius := rad ]
		ifFalse: [ radius := [ ^ IllegalState signal ] ]
	