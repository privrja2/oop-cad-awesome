as yet unclassified
shiftBy: aShiftPoint
	centerPoint translateBy: aShiftPoint.
	topLeftCorner translateBy: aShiftPoint.
	bottomRightCorner translateBy: aShiftPoint.