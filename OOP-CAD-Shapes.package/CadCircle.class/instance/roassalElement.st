as yet unclassified
roassalElement
	
	| ellipseShape circle |
	ellipseShape := RTEllipse new.
	ellipseShape size: radius * 2.
	circle := ellipseShape element.
	circle translateTo: centerPoint.

	^circle.
