accessing
centerPoint: aCenterPoint radius: aRadius
	centerPoint := aCenterPoint .
	radius := aRadius .
	width := aRadius * 2 .
	height := aRadius * 2 .
	topLeftCorner := CadAbsolutePoint new point: (aCenterPoint x - aRadius) @ (aCenterPoint y - aRadius) .
	bottomRightCorner := CadAbsolutePoint new point: (topLeftCorner x + width) @ (topLeftCorner y + width) .
	