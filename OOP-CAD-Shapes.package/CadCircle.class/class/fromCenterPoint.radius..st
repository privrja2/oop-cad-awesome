instance creation
fromCenterPoint: aCenterPoint radius: aRadius
	^ CadCircle new centerPoint: aCenterPoint radius: aRadius