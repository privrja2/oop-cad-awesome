updating
whenImportButtonClickedDo: aBlock
	importButtonClickedAction := aBlock.
	
"Example:
		cadView whenImportButtonClickedDo: [
			Transcript show: 'Clicked importButton'; cr.
		].
"
