updating
whenCommandAcceptedDo: aBlock
	commandAcceptedAction := aBlock.
	
"Example:
		cadView whenCommandAcceptedDo: [ :command |  
			cadView writeOutput: command.
		].
"
