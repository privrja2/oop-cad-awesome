updating
whenUndoButtonClickedDo: aBlock
	undoButtonClickedAction := aBlock.
	
"Example:
		cadView whenRedoButtonClickedDo: [
			Transcript show: 'Clicked redoButton'; cr.
		].
"

