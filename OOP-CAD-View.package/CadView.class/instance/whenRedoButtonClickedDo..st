updating
whenRedoButtonClickedDo: aBlock
	redoButtonClickedAction := aBlock.
	
"Example:
		cadView whenRedoButtonClickedDo: [
			Transcript show: 'Clicked redoButton'; cr.
		].
"

