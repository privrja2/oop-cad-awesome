updating
whenRemoveOneButtonClickedDo: aBlock
	removeOneButtonClickedAction := aBlock.
	
"Example:
		cadView whenRemoveOneButtonClickedDo: [
			Transcript show: 'Clicked removeOneButton'; cr.
		].
"

