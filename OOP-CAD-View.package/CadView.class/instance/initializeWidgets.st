initialization
initializeWidgets
	undoButton := self newButton.
	redoButton := self newButton.
	importButton := self newButton.
	exportButton := self newButton.
	removeOneButton := self newButton.
	removeAllButton := self newButton.
	lineButton := self newButton.
	rectangleButton := self newButton.
	circleButton := self newButton.
	
	textInput := self newTextInput.
	textInput ghostText: 'Write a command...'.
	filenameInput := self newTextInput.
	filenameInput ghostText: 'Write name of file...'.
	textOutput := self newTextInput.
	
	undoButton label: 'Undo'.
	redoButton label: 'Redo'.
	importButton label: 'Import file'.
	exportButton label: 'Export file'.
	removeOneButton label: 'Remove one'.
	removeAllButton label: 'Remove all'.
	lineButton label: 'Draw line'.
	rectangleButton label: 'Draw rectangle'.
	circleButton label: 'Draw circle'.

	roassalModel := self instantiate: RoassalModel.
	roassalModel
		script: [ :aView :aCanvas | 
			aCanvas color: Color white.
			view := aView.
			self initializeRoassalView: aView.
	].