updating
whenCircleButtonClickedDo: aBlock
	circleButtonClickedAction := aBlock.
	
"Example:
		cadView whenCircleButtonClickedDo: [
			Transcript show: 'Clicked circleButton'; cr.
		].
"
