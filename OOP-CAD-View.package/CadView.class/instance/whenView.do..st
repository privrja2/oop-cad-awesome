updating
whenView: anEvent do: aBlock
	view when: anEvent do: aBlock.

"Example:
		cadView whenView: TRMouseClick
		do: [ :event |  
			Transcript show: 'clicked '; show: event position asString; cr.
		].
"
