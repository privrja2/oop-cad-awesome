initialization
initializePresenter
	super initializePresenter.
	
	undoButton action: [ 
		self clickButton: undoButton.
		undoButtonClickedAction ifNotNil: [ undoButtonClickedAction value ].
		canvasLostFocusAction ifNotNil: [ canvasLostFocusAction value ].
	].
	redoButton action: [ 
		self clickButton: redoButton.
		redoButtonClickedAction ifNotNil: [ redoButtonClickedAction value ].
		canvasLostFocusAction ifNotNil: [ canvasLostFocusAction value ].
	].
	importButton action: [ 
		self clickButton: importButton.
		importButtonClickedAction ifNotNil: [ importButtonClickedAction value ].
		canvasLostFocusAction ifNotNil: [ canvasLostFocusAction value ].
	].
	exportButton action: [ 
		self clickButton: exportButton.
		exportButtonClickedAction ifNotNil: [ exportButtonClickedAction value ].
		canvasLostFocusAction ifNotNil: [ canvasLostFocusAction value ].
	].
	removeOneButton action: [ 
		self clickButton: removeOneButton.
		removeOneButtonClickedAction ifNotNil: [ removeOneButtonClickedAction value ].
		canvasLostFocusAction ifNotNil: [ canvasLostFocusAction value ].
	].
	removeAllButton action: [ 
		self clickButton: removeAllButton.
		removeAllButtonClickedAction ifNotNil: [ removeAllButtonClickedAction value ].
		canvasLostFocusAction ifNotNil: [ canvasLostFocusAction value ].
	].
	lineButton action: [ 
		self clickButton: lineButton.
		lineButtonClickedAction ifNotNil: [ lineButtonClickedAction value ].
		canvasLostFocusAction ifNotNil: [ canvasLostFocusAction value ].
	].
	rectangleButton action: [ 
		self clickButton: rectangleButton .
		rectangleButtonClickedAction ifNotNil: [ rectangleButtonClickedAction value ].
		canvasLostFocusAction ifNotNil: [ canvasLostFocusAction value ].
	].
	circleButton action: [ 
		self clickButton: circleButton.
		circleButtonClickedAction ifNotNil: [ circleButtonClickedAction value].
		canvasLostFocusAction ifNotNil: [ canvasLostFocusAction value ].
	].
	textOutput disable.
	textInput acceptBlock: [ 
		commandAcceptedAction ifNotNil: [ commandAcceptedAction value: textInput text ].
		canvasLostFocusAction ifNotNil: [ canvasLostFocusAction value ].
		textInput text: ''.
	].
	filenameInput autoAccept: true.