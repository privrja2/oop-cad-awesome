updating
whenLineButtonClickedDo: aBlock
	lineButtonClickedAction := aBlock.
	
"Example:
		cadView whenLineButtonClickedDo: [
			Transcript show: 'Clicked lineButton'; cr.
		].
"

