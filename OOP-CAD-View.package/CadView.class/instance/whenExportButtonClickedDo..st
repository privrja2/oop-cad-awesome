updating
whenExportButtonClickedDo: aBlock
	exportButtonClickedAction := aBlock.
	
"Example:
		cadView whenExportButtonClickedDo: [
			Transcript show: 'Clicked exportButton'; cr.
		].
"

