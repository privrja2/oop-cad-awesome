ui
clickButton: aButton
	| aButtonState |
	aButtonState := aButton state.
	
	lineButton state: false.
	lineButton label: 'Draw line'.
	rectangleButton state: false.
	circleButton state: false.
	removeOneButton state: false.
	
	((((aButton = lineButton) 
	or: (aButton = rectangleButton))
	or: (aButton = circleButton ))
	or: (aButton = removeOneButton ))
	ifTrue: [ 
		aButton state: aButtonState not.
		(aButton = lineButton) ifTrue: [ 
			(aButton state) ifTrue: [ 
				aButton label: 'Finish drawing'.
			] ifFalse: [ 
				aButton label: 'Draw line'.
			]
		 ]
	].
	

	
