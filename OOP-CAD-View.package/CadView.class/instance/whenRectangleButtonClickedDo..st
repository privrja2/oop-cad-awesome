updating
whenRectangleButtonClickedDo: aBlock
	rectangleButtonClickedAction := aBlock.
	
"Example:
		cadView whenRectangleButtonClickedDo: [
			Transcript show: 'Clicked rectangleButton'; cr.
		].
"
