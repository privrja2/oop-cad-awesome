updating
whenCanvasLostFocusDo: aBlock
	canvasLostFocusAction := aBlock.
	
"Example:
		cadView whenCanvasLostFocusDo: [ 
			Transcript show: 'Canvas lost focus.'; cr.
		].
"
