updating
whenRemoveAllButtonClickedDo: aBlock
	removeAllButtonClickedAction := aBlock.
	
"Example:
		cadView whenRemoveAllButtonClickedDo: [
			Transcript show: 'Clicked removeAllButton'; cr.
		].
"

