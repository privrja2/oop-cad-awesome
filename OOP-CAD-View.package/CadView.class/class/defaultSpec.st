specs
defaultSpec
	|topMenuHeight topMenuWidth sideMenuHeight sideMenuWidth textOutputHeight textInputHeight|
	topMenuWidth := 0.6.	
	topMenuHeight := 0.05.
	sideMenuWidth := 0.12.	
	sideMenuHeight := 0.5.
	textOutputHeight := 0.2.
	textInputHeight := 0.05.
	
	^ SpecLayout composed 
		"Top menu row"
		newRow: [ :row | row add: #undoButton; add: #redoButton; add: #removeOneButton; add: #removeAllButton]
		origin: 0 @ 0 corner: topMenuWidth @ topMenuHeight ;
		"Import menu row"
		newRow: [ :row | row add: #importButton; add:	#exportButton; add: #filenameInput ]
		origin: 0 @ topMenuHeight corner: topMenuWidth @ (topMenuHeight * 2) ;
		"Side menu column"
		newColumn: [ :column | column add: #lineButton; add: #rectangleButton; add: #circleButton. ]
		origin: 0 @ (topMenuHeight * 2) corner: sideMenuWidth @ sideMenuHeight;
		"Roassal view"
		newRow: [ :row | row add: #roassalModel ]
		origin: sideMenuWidth @ (topMenuHeight * 2) corner: 1 @ (1-textInputHeight-textOutputHeight);
		"Text output row"
		newRow: [ :row | row add: #textOutput ]
		origin: sideMenuWidth @ (1-textInputHeight-textOutputHeight) corner: 1 @ (1-textInputHeight);
		"Text input row"
		newRow: [ :row | row add: #textInput ]
		origin: sideMenuWidth @ (1-textInputHeight)  corner: 1 @ 1;
		yourself	
