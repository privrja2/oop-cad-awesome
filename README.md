# CAD

## Assignment

The goal is to create very simple version of CAD (computer aided design) tool inspired by the early versions of AutoCAD 1.0.
Detailed information as well as a skeleton of the user interface in Java is available on [gitlab](https://gitlab.fit.cvut.cz/BI-OOP/cad-java-skeleton).

## Specification of Assignment

The application allows one to draw the following shapes:
* polylines
* rectangles
* circles

Next to drawing shapes, the application provide additional actions:
* save shapes into a file
* load shapes from a file
* remove a shape
* move a shape
* remove all shapes
* list drawn shapes
* quit the application
* undo the last action
* redo the last action

The drawing can be done by either typing commands into the command panel or by using mouse. Point 0@0 is set to top left corner.
Detailed specification of commands is [here](https://gitlab.fit.cvut.cz/BI-OOP/cad-java-skeleton#actions).

# Documentation

# Installation

## Requirements

Pharo 6.1 with Roassal and Iceberg

## Installation
To install CAD you need to clone this [repository](https://gitlab.fit.cvut.cz/privrja2/oop-cad-awesome) to your image of pharo with little help with Iceberg.
Then you have all needed in your pharo image.

Or use source code in zip file in .st format. But from unknown reason CadSavedOutputTestView change its superclass, when importing to pharo. In .st file is setup correct, but after import, change CadView to Object.

# Architecture

Base of Application is MVC architecture. CadModel, CadView and CadController. CadModel holds a CommandHandler for storing information about commands, for redo and undo operations.

![MVC and CommandHandler](images/cad-mvc.png)

CadCommand is abstract class (based on command pattern) and their subclasses need to implement execute and revert. There are 10 commands, like line, rect, circle, save, load, ...

![Commands](images/cad-commands.png)

But only 3 that needs to have information about shape. So there is a CadShape class that is a super class for that 3 shapes (line, rect, circle).

![Shapes](images/cad-shapes.png)

Parsing user input is doing via parser combinators pattern. There are many parsers compose together from smaller to biger ones. Parsers have only one method which return ParseResult. ParserResult is abstract class. Their subclasses is Accept and Reject, which contain information about result of parsing user input. 

![Parsers](images/cad-parsers.png)

# List of Design Patterns

Parser Combinators - Action Parser and other smaller parsers

Polymorphism (Command Pattern) - CadCommand, and their subclasses, different behaviour for executing and reverting

MVC architecture - CadModel, CadView, CadController

Factory - ParseResultFactory, creating instances for Reject and Accept

# Examples

## Start CAD
```st
"Open Transcript to view logs."
Transcript clear; open.

"Create new instance of CadController and open it."
CadController new open.
```

## Mouse interface

### Drawing Line

To draw line you have to click on Draw line button. Then click on drawing area all points of line. Than click back on Draw line button with change text to Finish Drawing. Now system draw the line.

### Drawing Rectangle

To draw a rectangle click on Draw rectangle button. Then click on drawing area two times, specify left upper conrner and right bottom corner of rectangle.

### Drawing Circle

To draw circle click on Draw circle button. Then click on drawing area to specify center and then click again to specify radius from center.

## Basic operations with CadView

### Open CadView

```st
"Create new instance of CadView and open it."
cadView := CadView new.
cadView open.

"Create model and assign it to the view"
cadModel := CadModel new.
cadView cadModel: cadModel.
```

### Create square at [0,0]

```st
boxShape := RTBox new.
boxShape size: 50.
box := boxShape element.
box translateTo: 0@0.
box @ RTDraggable.

cadModel addElement: box.
```

### Create circle at [100,100]

```st
ellipseShape := RTEllipse new.
ellipseShape size: 50.
circle := ellipseShape element.
circle translateTo: 100@100.
circle @ RTDraggable.

cadModel addElement: circle.
```

### Register drag events on element

```st
element when: TRMouseDragStart do: [ :e | 
	...
]
element when: TRMouseDragStop do: [ :e | 
	...
]
```

### Register click event on element to remove it

```
element when: TRMouseClick do: [ :event | 
	view removeOneButton state ifTrue: [
		"create remove command, access element like this:
			event shape element"
	]
```

### Create line

```st
ellipseShape := RTEllipse new.
e1 := ellipseShape element.
e1 translateTo: -30@50.

e2 := ellipseShape element.
e2 translateTo: 60@ -120.

edge := RTLine edgeFrom: e1 to: e2.
cadModel addElement: edge.
```

### Register event handler block when command is accepted by the input

```st
cadView whenCommandAcceptedDo: [ :command | 
        cadView writeOutput: command.
    ].
```

### Register event handler block to capture clicks on a button

```st
cadView whenCircleButtonClickedDo: [ :state |  
        Transcript show: 'Clicked circleButton with state:'; show: state; cr.
    ].
```

### Register event handler block to capture clicks inside Roassal view

```st
cadView whenView: TRMouseClick do: [ :event |  
        Transcript show: 'clicked '; show: event position asString; cr.
    ].
```


