accessing
parse: aText
	| circle space pointParser sourcePoint destinationPoint radius |
	
	"parse circle"
	circle := StringParser new parse: aText with: 'circle ' .
	circle isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectCircle: aText ] .
		
	"parse point"
	pointParser := PointParser new .
	sourcePoint := pointParser parse: circle remainder .
	sourcePoint isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectCircle: aText ] .
	
	"Skip space"
	space := SpaceParser new parse: sourcePoint remainder .
	space isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectCircle: aText ] .
	
	destinationPoint := pointParser parse: space remainder .
	destinationPoint isAccepted 
		ifTrue: [ ^ ParseResultFactory buildAcceptCircleFromTopLeftCorner: sourcePoint result bottomRightCorner: destinationPoint result reminder: destinationPoint remainder ] .
		
	"Variant with radius"
	radius := NatParser new parse: space remainder .
	radius isAccepted
		ifTrue: [ ^ ParseResultFactory buildAcceptCircleFromPoint: sourcePoint result radius: radius result reminder: radius remainder ] .
	^ ParseResultFactory buildRejectCircle: aText 