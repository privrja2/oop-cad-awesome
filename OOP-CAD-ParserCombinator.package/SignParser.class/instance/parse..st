accessing
parse: aText
	aText isEmpty
		ifTrue: [ ^ ParseResultFactory buildRejectTextEmptyFrom: aText ] .
	
	((aText at: 1) asString matchesRegex: '[+-]')
		ifTrue: [ ^ ParseResultFactory buildAcceptFrom: aText ]
		ifFalse: [ ^ Reject new errorMessage: 'Not match sign'; remainder: aText ]