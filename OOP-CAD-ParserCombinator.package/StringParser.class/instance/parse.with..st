parsing
parse: aText with: aString
	| charParser nextChar parsedString position |
	(aString isEmpty or: [ aText size < aString size ])
		ifTrue: [ ^ ParseResultFactory buildRejectString: aText ] .
	parsedString := '' .
	position := 1 .
	charParser := CharParser new .
	nextChar := charParser parse: aText with: (aString at: position) .
	[ nextChar isAccepted ]
		whileTrue: [
			position := position + 1 .
			parsedString := parsedString , nextChar result asString .
			nextChar := aString size < position
				ifTrue: [ charParser parse: nextChar remainder with: '' ]
				ifFalse: [ charParser parse: nextChar remainder with: (aString at: position) ]
		] .
	parsedString = aString
		ifTrue: [ ^ Accept new result: aString; remainder: nextChar remainder ]
		ifFalse: [ ^ ParseResultFactory buildRejectString: aText ]