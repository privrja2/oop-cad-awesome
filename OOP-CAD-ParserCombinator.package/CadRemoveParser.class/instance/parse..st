accessing
parse: aText
	| remove point |
	remove := StringParser new parse: aText with: 'remove ' .
	remove isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectRemove: aText ] .
		
	point := PointParser new parse: remove remainder .
	point isAccepted
		ifTrue: [ ^ Accept new result: (CadRemoveCommand new point: point result); remainder: point remainder ] .
	^ ParseResultFactory buildRejectRemove: aText