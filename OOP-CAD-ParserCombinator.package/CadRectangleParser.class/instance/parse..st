accessing
parse: aText
	| rect space pointParser spaceParser natParser sourcePoint destinationPoint width height |
	
	"parse rectangle"
	rect := StringParser new parse: aText with: 'rect ' .
	rect isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectRectangle: aText ] .
	
	"parse point"
	pointParser :=	PointParser new .
	sourcePoint := pointParser parse: rect remainder .
	sourcePoint isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectRectangle: aText ] .
	
	"Skip space"
	spaceParser := SpaceParser new .
	space := spaceParser parse: sourcePoint remainder .
	space isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectRectangle: aText ] .
		
	destinationPoint := pointParser parse: space remainder .
	destinationPoint isAccepted
		ifTrue: [ ^ Accept new result: (CadRectangleCommand new shape: (CadRectangle fromTopLeftCorner: sourcePoint result bottomRightCorner: destinationPoint result)); remainder: destinationPoint remainder ] .	
		
	"variant with width & height"
	natParser := NatParser new .
	width := natParser parse: space remainder .
	width isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectRectangle: aText ] .
	
	space := spaceParser parse: width remainder .
	space isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectRectangle: aText ] .
	
	height := natParser parse: space remainder .
	height isAccepted 
		ifTrue: [ ^ Accept new result: (CadRectangleCommand new shape: (CadRectangle fromTopLeftCorner: sourcePoint result width: width result height: height result)); remainder: height remainder ] .
	^ ParseResultFactory buildRejectRectangle: aText
