accessing
parse: aText
	aText isEmpty
		ifTrue: [ ^ ParseResultFactory buildRejectTextEmptyFrom: aText ] .
	
	((aText at: 1) asString matchesRegex: '[1-9]')
		ifTrue: [ ^ ParseResultFactory buildAcceptFrom: aText ]
		ifFalse: [ ^ Reject new errorMessage: 'Not match 1-9'; remainder: aText ]
	