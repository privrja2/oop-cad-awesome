accessing
parse: aText
	aText isEmpty
		ifTrue: [ ^ ParseResultFactory buildRejectTextEmptyFrom: aText ] .
	
	((aText at: 1) asString matchesRegex: '[0-9]')
		ifTrue: [ ^ ParseResultFactory buildAcceptFrom: aText ]
		ifFalse: [ ^ ParseResultFactory buildRejectNextNum: aText ]