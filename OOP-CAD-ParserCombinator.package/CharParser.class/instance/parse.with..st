parsing
parse: aText with: aCharacter
	aText isEmpty
	ifTrue: [ ^ ParseResultFactory buildRejectTextEmptyFrom: aText ] .
	
	(aText at: 1) asCharacter = aCharacter
		ifTrue: [ ^ ParseResultFactory buildAcceptFrom: aText ]
		ifFalse: [ ^ ParseResultFactory buildRejectCharText: aText char: aCharacter ]
	