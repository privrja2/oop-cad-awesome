accessing
parse: aText
	"!!!ALERT RECURSION!!!"

	| end point points space |
	point := PointParser new parse: aText .
	point isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectPoints: aText ] .

	end := EndStringParser new parse: point remainder .
	end isAccepted
		ifTrue: [ ^ Accept new result: (OrderedCollection new add: point result; yourself); remainder: '' ] .

	space := SpaceParser new parse: point remainder .
	space isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectPoints: aText ] .
	
	points := PointsParser new parse: space remainder .
	points isAccepted
		ifTrue: [ ^ Accept new result: (points result addFirst: point result; yourself); remainder: points remainder ] .
	^ points remainder: aText
	