comparing
= anAccept
	(anAccept isMemberOf: Accept)
		ifTrue: [ ^ (self result = anAccept result) and: [ self remainder = anAccept remainder ] ]
		ifFalse: [ ^ false ]