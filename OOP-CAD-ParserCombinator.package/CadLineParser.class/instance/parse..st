accessing
parse: aText
	| line point space points |
	line := StringParser new parse: aText with: 'line ' .
	line isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectLine: aText ] .
	
	point := PointParser new parse: line remainder .
	point isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectLine: aText ] .
	
	space := SpaceParser new parse: point remainder .
	space isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectLine: aText ] .
	
	points := PointsParser new parse: space remainder .
	points isAccepted
		ifTrue: [ ^ Accept new result: (CadLineCommand new shape: (CadLine new startPoint: point result points: points result)); remainder: points remainder ] .
	^ ParseResultFactory buildRejectLine: aText
