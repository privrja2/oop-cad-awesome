accessing
parse: aText
	| list end space point |
	list := StringParser new parse: aText with: 'ls' .
	list isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectList: aText ] .
	
	end := EndStringParser new parse: list remainder .
	end isAccepted
		ifTrue: [ ^ Accept new result: CadListCommand new; remainder: '' ] .
	space := SpaceParser new parse: list remainder .
	space isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectList: aText ] .
		
	point := PointParser new parse: space remainder .
	point isAccepted
		ifTrue: [ ^ Accept new result: (CadListCommand new point: point result); remainder: point remainder ] .
	^ ParseResultFactory buildRejectList: aText
	
	