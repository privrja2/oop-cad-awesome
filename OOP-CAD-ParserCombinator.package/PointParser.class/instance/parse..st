accessing
parse: aText
	| absPoint relPoint |
	absPoint := AbsolutePointParser new parse: aText .
	absPoint isAccepted
		ifTrue: [ ^ absPoint ] .
	relPoint := RelativePointParser new parse: aText .
	relPoint isAccepted
		ifTrue: [ ^ relPoint  ] .
	^ ParseResultFactory buildRejectPoint: aText