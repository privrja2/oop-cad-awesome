tests
testFirstNumParser
	self assert: (FirstNumParser new parse: '15') equals: (Accept new result: $1; remainder: '5').
	self assert: (FirstNumParser new parse: '015') equals: (Reject new errorMessage: 'Not match 1-9'; remainder: '015').
	self assert: (FirstNumParser new parse: 's15') equals: (Reject new errorMessage: 'Not match 1-9'; remainder: 's15').
	self assert: (FirstNumParser new parse: '') equals: (Reject new errorMessage: 'Text is empty'; remainder: '').