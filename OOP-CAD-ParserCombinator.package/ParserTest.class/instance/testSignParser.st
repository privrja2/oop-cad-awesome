tests
testSignParser
	self assert: (SignParser new parse: '+') equals: (Accept new result: $+; remainder: '').
	self assert: (SignParser new parse: '-') equals: (Accept new result: $-; remainder: '').
	self assert: (SignParser new parse: '-5') equals: (Accept new result: $-; remainder: '5').
	self assert: (SignParser new parse: 'f') equals: (Reject new errorMessage: 'Not match sign'; remainder: 'f').
	self assert: (SignParser new parse: '9') equals: (Reject new errorMessage: 'Not match sign'; remainder: '9').
	self assert: (SignParser new parse: '') equals: (Reject new errorMessage: 'Text is empty'; remainder: '').