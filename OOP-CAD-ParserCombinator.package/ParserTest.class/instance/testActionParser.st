tests
testActionParser
	self assert: (CadActionParser new parse: 'circle 15,25 5') equals: (Accept new result: (CadCircleCommand new shape: (CadCircle fromTopLeftCorner: (CadAbsolutePoint new point: 10@20) bottomRightCorner: (CadAbsolutePoint new point: 20@30))); remainder: '').
	self assert: (CadActionParser new parse: 'circle 10,20 20,30 c') equals: (Reject new errorMessage: 'Not match ACTION'; remainder: 'circle 10,20 20,30 c').
	self assert: (CadActionParser new parse: 'remove 8,6') equals: (Accept new result: (CadRemoveCommand new point: (CadAbsolutePoint new point: 8@6)); remainder: '').
	self assert: (CadActionParser new parse: 'remove +8,-2') equals: (Accept new result: (CadRemoveCommand new point: (CadRelativePoint new point: 8@(-2))); remainder: '').
	self assert: (CadActionParser new parse: 'move 7,8 5,9') equals: (Accept new result: (CadMoveCommand new point: (CadAbsolutePoint new point: 7@8); newPoint: (CadAbsolutePoint new point: 5@9)); remainder: '').
	self assert: (CadActionParser new parse: ' move 7,8 5,9') equals: (Accept new result: (CadMoveCommand new point: (CadAbsolutePoint new point: 7@8); newPoint: (CadAbsolutePoint new point: 5@9)); remainder: '').
	self assert: (CadActionParser new parse: 'circe 15,25 5') equals: (Reject new errorMessage: 'Not match ACTION'; remainder: 'circe 15,25 5').
	self assert: (CadActionParser new parse: 'remove') equals: (Reject new errorMessage: 'Not match ACTION'; remainder: 'remove').
	self assert: (CadActionParser new parse: 'c move 7,8 5,9') equals: (Reject new errorMessage: 'Not match ACTION'; remainder: 'c move 7,8 5,9').
	self assert: (CadActionParser new parse: 'new') equals: (Reject new errorMessage: 'Not match ACTION'; remainder: 'new').
	self assert: (CadActionParser new parse: '') equals: (Reject new errorMessage: 'Not match ACTION'; remainder: '').
	self assert: (CadActionParser new parse: ' ') equals: (Reject new errorMessage: 'Not match ACTION'; remainder: ' ').