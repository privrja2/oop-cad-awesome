tests
testRectangleParser
	self assert: (CadRectangleParser new parse: 'rect 15,25 5 8') equals: (Accept new result: (CadRectangleCommand new shape: (CadRectangle fromTopLeftCorner: (CadAbsolutePoint new point: 15@25) bottomRightCorner: (CadAbsolutePoint new point: 20@33))); remainder: '').
	self assert: (CadRectangleParser new parse: 'rect 10,20 20,30 c') equals: (Accept new result: (CadRectangleCommand new shape: (CadRectangle fromTopLeftCorner: (CadAbsolutePoint new point: 10@20) bottomRightCorner: (CadAbsolutePoint new point: 20@30))); remainder: ' c').
	self assert: (CadRectangleParser new parse: 'rect 15,25 5 5') equals: (Accept new result: (CadRectangleCommand new shape: (CadRectangle fromTopLeftCorner: (CadAbsolutePoint new point: 15@25) bottomRightCorner: (CadAbsolutePoint new point: 20@30))); remainder: '').
	self assert: (CadRectangleParser new parse: 'rectangle 15,25 5 5') equals: (Reject new errorMessage: 'Not match RECTANGLE'; remainder: 'rectangle 15,25 5 5').
	self assert: (CadRectangleParser new parse: 'rect 15,25 +20,+33') equals: (Accept new result: (CadRectangleCommand new shape: (CadRectangle fromTopLeftCorner: (CadAbsolutePoint new point: 15@25) bottomRightCorner: (CadRelativePoint new point: 20@33))); remainder: '').
	self assert: (CadRectangleParser new parse: 'rect 15,25 +5 -8') equals: (Reject new errorMessage: 'Not match RECTANGLE'; remainder: 'rect 15,25 +5 -8'). 
	self assert: (CadRectangleParser new parse: '') equals: (Reject new errorMessage: 'Not match RECTANGLE'; remainder: '').