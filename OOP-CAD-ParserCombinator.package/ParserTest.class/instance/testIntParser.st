tests
testIntParser
	self assert: (IntParser new parse: '+923') equals: (Accept new result: 923; remainder: '').
	self assert: (IntParser new parse: '-123') equals: (Accept new result: -123; remainder: '').
	self assert: (IntParser new parse: 'd') equals: (Reject new errorMessage: 'Not match INT'; remainder: 'd').
	self assert: (IntParser new parse: '123') equals: (Reject new errorMessage: 'Not match INT'; remainder: '123').
	self assert: (IntParser new parse: '--123') equals: (Reject new errorMessage: 'Not match INT'; remainder: '--123').