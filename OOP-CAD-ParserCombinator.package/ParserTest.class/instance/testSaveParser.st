tests
testSaveParser
	self assert: (CadSaveParser  new parse: 'save game') equals: (Accept new result: (CadSaveCommand new name: 'game'); remainder: '').
	self assert: (CadSaveParser new parse: 'save gamecad') equals: (Accept new result: (CadSaveCommand new name: 'gamecad'); remainder: '').
	self assert: (CadSaveParser new parse: 'save game.cad') equals: (Accept new result: (CadSaveCommand new name: 'game.cad'); remainder: '').
		self assert: (CadSaveParser new parse: 'save ./data/game.cad') equals: (Accept new result: (CadSaveCommand new name: './data/game.cad'); remainder: '').
		self assert: (CadSaveParser new parse: 'save data\game.cad') equals: (Accept new result: (CadSaveCommand new name: 'data\game.cad'); remainder: '').
	self assert: (CadSaveParser new parse: 'save game.cad ') equals: (Reject new errorMessage: 'Not match SAVE'; remainder: 'save game.cad ').
	self assert: (CadSaveParser new parse: ' ') equals: (Reject new errorMessage: 'Not match SAVE'; remainder: ' ').
	self assert: (CadSaveParser new parse: '') equals: (Reject new errorMessage: 'Not match SAVE'; remainder: '').