tests
testCharParser
	self assert: (CharParser new parse: 'line 8 5' with: $l) equals: (Accept new result: $l; remainder: 'ine 8 5').
	self assert: (CharParser new parse: 'line 8 5' with: '') equals: (Reject new errorMessage: 'Not match prefered character '; remainder: 'line 8 5').
	self assert: (CharParser new parse: 'line 8 5' with: $s) equals: (Reject new errorMessage: 'Not match prefered character s'; remainder: 'line 8 5').
	self assert: (CharParser new parse: '' with: $s) equals: (Reject new errorMessage: 'Text is empty'; remainder: '').