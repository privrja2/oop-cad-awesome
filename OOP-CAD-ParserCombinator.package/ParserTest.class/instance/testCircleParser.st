tests
testCircleParser
	self assert: (CadCircleParser new parse: 'circle 15,25 5') equals: (Accept new result: (CadCircleCommand new shape: (CadCircle fromTopLeftCorner: (CadAbsolutePoint new point: 10@20) bottomRightCorner: (CadAbsolutePoint new point: 20@30))); remainder: '').
	self assert: (CadCircleParser new parse: 'circle 10,20 20,30 c') equals: (Accept new result: (CadCircleCommand new shape: (CadCircle fromCenterPoint: (CadAbsolutePoint new point: 15@25) radius: 5)); remainder: ' c').
	self assert: (CadCircleParser new parse: 'circe 15,25 5') equals: (Reject new errorMessage: 'Not match CIRCLE'; remainder: 'circe 15,25 5').
	self assert: (CadCircleParser new parse: '') equals: (Reject new errorMessage: 'Not match CIRCLE'; remainder: '').