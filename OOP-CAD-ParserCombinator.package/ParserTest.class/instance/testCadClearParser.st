tests
testCadClearParser
	self assert: (CadClearParser new parse: 'clear') equals: (Accept new result: CadClearCommand new; remainder: '').
	self assert: (CadClearParser new parse: 'clea') equals: (Reject new errorMessage: 'Not match CLEAR'; remainder: 'clea').
	self assert: (CadClearParser new parse: ' ') equals: (Reject new errorMessage: 'Not match CLEAR'; remainder: ' ').
	self assert: (CadClearParser new parse: '') equals: (Reject new errorMessage: 'Not match CLEAR'; remainder: '').