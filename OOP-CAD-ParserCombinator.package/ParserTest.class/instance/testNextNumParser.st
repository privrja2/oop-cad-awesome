tests
testNextNumParser
	self assert: (NextNumParser  new parse: '15') equals: (Accept new result: $1; remainder: '5').
	self assert: (NextNumParser new parse: '015') equals: (Accept new result: $0; remainder: '15').
	self assert: (NextNumParser new parse: 's15') equals: (Reject new errorMessage: 'Not match 0-9'; remainder: 's15').
	self assert: (NextNumParser new parse: '') equals: (Reject new errorMessage: 'Text is empty'; remainder: '').