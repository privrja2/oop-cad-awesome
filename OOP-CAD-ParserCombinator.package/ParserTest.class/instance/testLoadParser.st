tests
testLoadParser
	self assert: (CadLoadParser new parse: 'load game') equals: (Accept new result: (CadLoadCommand new name: 'game'); remainder: '').
	self assert: (CadLoadParser new parse: 'load gamecad') equals: (Accept new result: (CadLoadCommand new name: 'gamecad'); remainder: '').
	self assert: (CadLoadParser new parse: 'load game.cad') equals: (Accept new result: (CadLoadCommand new name: 'game.cad'); remainder: '').
	self assert: (CadLoadParser new parse: 'load data\game.cad') equals: (Accept new result: (CadLoadCommand new name: 'data\game.cad'); remainder: '').
	self assert: (CadLoadParser new parse: 'load ../data/game.cad') equals: (Accept new result: (CadLoadCommand new name: '../data/game.cad'); remainder: '').
	self assert: (CadLoadParser new parse: 'load game.cad ') equals: (Reject new errorMessage: 'Not match LOAD'; remainder: 'load game.cad ').
	self assert: (CadLoadParser new parse: 'save game') equals: (Reject new errorMessage: 'Not match LOAD'; remainder: 'save game').
	self assert: (CadLoadParser new parse: ' ') equals: (Reject new errorMessage: 'Not match LOAD'; remainder: ' ').
	self assert: (CadLoadParser new parse: '') equals: (Reject new errorMessage: 'Not match LOAD'; remainder: '').