tests
testRelativePointParser
	self assert: (RelativePointParser new parse: '+15,+25') equals: (Accept new result: (CadRelativePoint new point: 15@25); remainder: '').
	self assert: (RelativePointParser new parse: '+1,-18') equals: (Accept new result: (CadRelativePoint new point: 1@(-18)); remainder: '').
	self assert: (RelativePointParser new parse: '15,') equals: (Reject new errorMessage: 'Not match RELATIVE_POINT'; remainder: '15,').
	self assert: (RelativePointParser new parse: 's15') equals: (Reject new errorMessage: 'Not match RELATIVE_POINT'; remainder: 's15').