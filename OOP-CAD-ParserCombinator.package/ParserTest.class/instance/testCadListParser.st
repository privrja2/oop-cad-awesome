tests
testCadListParser
	self assert: (CadListParser new parse: 'ls') equals: (Accept new result: CadListCommand new; remainder: '').
	self assert: (CadListParser new parse: 'ls 88,0') equals: (Accept new result: (CadListCommand new point: (CadAbsolutePoint new point: 88@0)); remainder: '').
	self assert: (CadListParser new parse: 'ls -9,-5') equals: (Accept new result: (CadListCommand new point: (CadRelativePoint new point: -9@(-5))); remainder: '').
	self assert: (CadListParser new parse: 'list') equals: (Reject new errorMessage: 'Not match LS'; remainder: 'list').
	self assert: (CadListParser new parse: ' ') equals: (Reject new errorMessage: 'Not match LS'; remainder: ' ').
	self assert: (CadListParser new parse: '') equals: (Reject new errorMessage: 'Not match LS'; remainder: '').