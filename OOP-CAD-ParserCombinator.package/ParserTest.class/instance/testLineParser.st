tests
testLineParser
	self assert: (CadLineParser new parse: 'line 15,25 5,8') equals: (Accept new result: (CadLineCommand new shape: (CadLine new startPoint: (CadAbsolutePoint new point: 15@25) points: {(CadAbsolutePoint new point: 5@8)} asOrderedCollection)); remainder: '').
	self assert: (CadLineParser new parse: 'line 15,25 -5,-5') equals: (Accept new result: (CadLineCommand new shape: (CadLine new startPoint: (CadAbsolutePoint new point: 15@25) points: {(CadRelativePoint new point: -5@(-5))})); remainder: '').
	self assert: (CadLineParser new parse: 'line 15,25 +20,+33') equals: (Accept new result: (CadLineCommand new shape: (CadLine new startPoint: (CadAbsolutePoint new point: 15@25) points: {(CadRelativePoint new point: 20@33)})); remainder: '').
	self assert: (CadLineParser new parse: 'line 15,25 20,33 -8,+9 +5,-9') equals: (Accept new result: (CadLineCommand new shape: (CadLine new startPoint: (CadAbsolutePoint new point: 15@25) points: {(CadAbsolutePoint new point: 20@33). (CadRelativePoint new point: -8@9) . (CadRelativePoint new point: 5@(-9))})); remainder: '').
	self assert: (CadLineParser new parse: 'line 10,20') equals: (Reject new errorMessage: 'Not match LINE'; remainder: 'line 10,20').
	self assert: (CadLineParser new parse: 'line 10,20 20,30 c') equals: (Reject new errorMessage: 'Not match LINE'; remainder: 'line 10,20 20,30 c').
	self assert: (CadLineParser new parse: 'lin 15,25 5 5') equals: (Reject new errorMessage: 'Not match LINE'; remainder: 'lin 15,25 5 5').
	self assert: (CadLineParser new parse: 'line 15,25 5 5') equals: (Reject new errorMessage: 'Not match LINE'; remainder: 'line 15,25 5 5').
	self assert: (CadLineParser new parse: 'line 15,25 +5 -8') equals: (Reject new errorMessage: 'Not match LINE'; remainder: 'line 15,25 +5 -8'). 
	self assert: (CadLineParser new parse: '') equals: (Reject new errorMessage: 'Not match LINE'; remainder: '').