tests
testAbsolutePointParser
	self assert: (AbsolutePointParser new parse: '15,25') equals: (Accept new result: (CadAbsolutePoint new point: 15@25); remainder: '').
	self assert: (AbsolutePointParser new parse: '15,') equals: (Reject new errorMessage: 'Not match ABSOLUTE_POINT'; remainder: '15,').
	self assert: (AbsolutePointParser new parse: 's15') equals: (Reject new errorMessage: 'Not match ABSOLUTE_POINT'; remainder: 's15').
	self assert: (AbsolutePointParser new parse: '1,-18') equals: (Reject new errorMessage: 'Not match ABSOLUTE_POINT'; remainder: '1,-18').