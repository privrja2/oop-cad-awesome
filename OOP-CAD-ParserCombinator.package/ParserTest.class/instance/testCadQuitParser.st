tests
testCadQuitParser
	self assert: (CadQuitParser new parse: 'quit') equals: (Accept new result: CadQuitCommand new; remainder: '').
	self assert: (CadQuitParser new parse: 'qui') equals: (Reject new errorMessage: 'Not match QUIT'; remainder: 'qui').
	self assert: (CadQuitParser new parse: 'qut') equals: (ParseResultFactory buildRejectQuit: 'qut').
	self assert: (CadQuitParser new parse: ' ') equals: (Reject new errorMessage: 'Not match QUIT'; remainder: ' ').
	self assert: (CadQuitParser new parse: '') equals: (Reject new errorMessage: 'Not match QUIT'; remainder: '').