tests
testMoveParser
	self assert: (CadMoveParser  new parse: 'move 7,8 5,9') equals: (Accept new result: (CadMoveCommand new point: (CadAbsolutePoint new point: 7@8); newPoint: (CadAbsolutePoint new point: 5@9)); remainder: '').
	self assert: (CadMoveParser new parse: 'move 6,4 +0,-4') equals: (Accept new result: (CadMoveCommand new point: (CadAbsolutePoint new point: 6@4); newPoint: (CadRelativePoint new point: 0@(-4))); remainder: '').
	self assert: (CadMoveParser new parse: 'move 6,4, +0,-4') equals: (Reject new errorMessage: 'Not match MOVE'; remainder: 'move 6,4, +0,-4').
	self assert: (CadMoveParser new parse: 'mov 6,4 0,4') equals: (Reject new errorMessage: 'Not match MOVE'; remainder: 'mov 6,4 0,4').
	self assert: (CadMoveParser new parse: 'move game.cad') equals: (Reject new errorMessage: 'Not match MOVE'; remainder: 'move game.cad').
	self assert: (CadMoveParser new parse: ' ') equals: (Reject new errorMessage: 'Not match MOVE'; remainder: ' ').
	self assert: (CadMoveParser new parse: '') equals: (Reject new errorMessage: 'Not match MOVE'; remainder: '').