tests
testNatParser
	self assert: (NatParser new parse: '123') equals: (Accept new result: 123; remainder: '').
	self assert: (NatParser new parse: '0') equals: (Accept new result: 0; remainder: '').
	self assert: (NatParser new parse: '0s') equals: (Accept new result: 0; remainder: 's').
	self assert: (NatParser new parse: '00') equals: (Reject new errorMessage: 'Not match NAT'; remainder: '00').
	self assert: (NatParser new parse: '01') equals: (Reject new errorMessage: 'Not match NAT'; remainder: '01').
	self assert: (NatParser new parse: 'd') equals: (Reject new errorMessage: 'Not match NAT'; remainder: 'd').
	self assert: (NatParser new parse: '+123') equals: (Reject new errorMessage: 'Not match NAT'; remainder: '+123').
	self assert: (NatParser new parse: '-123') equals: (Reject new errorMessage: 'Not match NAT'; remainder: '-123').
	self assert: (NatParser new parse: '--123') equals: (Reject new errorMessage: 'Not match NAT'; remainder: '--123').