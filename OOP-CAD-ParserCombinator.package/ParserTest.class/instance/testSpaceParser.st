tests
testSpaceParser
	self assert: (SpaceParser new parse: ' ') equals: (Accept new result: Character space; remainder: '').
	self assert: (SpaceParser new parse: ' Geralt of Rivia') equals: (Accept new result: Character space; remainder: 'Geralt of Rivia').
	self assert: (SpaceParser new parse: 'Ciri, Geralt, Yennefer, Triss, Marigold') equals: (Reject new errorMessage: 'Not match prefered character  '; remainder: 'Ciri, Geralt, Yennefer, Triss, Marigold').
	self assert: (SpaceParser new parse: '') equals: (Reject new errorMessage: 'Text is empty'; remainder: '').