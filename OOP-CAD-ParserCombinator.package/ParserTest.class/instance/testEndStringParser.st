tests
testEndStringParser
	self assert: (EndStringParser new parse: '') equals: (Accept new result: ''; remainder: '').
	self assert: (EndStringParser new parse: 'x') equals: (Reject new errorMessage: 'Not match '''''; remainder: 'x').
	self assert: (EndStringParser new parse: 'xcad ') equals: (Reject new errorMessage: 'Not match '''''; remainder: 'xcad ').
	self assert: (EndStringParser new parse: 'x.cad') equals: (Reject new errorMessage: 'Not match '''''; remainder: 'x.cad').
	self assert: (EndStringParser new parse: ' ') equals: (Reject new errorMessage: 'Not match '''''; remainder: ' ').