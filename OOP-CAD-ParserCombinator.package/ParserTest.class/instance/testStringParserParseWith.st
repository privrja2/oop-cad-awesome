tests
testStringParserParseWith
	self assert: (StringParser new parse: 'h' with: 'h') equals: (Accept new result: 'h'; remainder: '') .
	self assert: (StringParser new parse: 'h ' with: 'h') equals: (Accept new result: 'h'; remainder: ' ') .
	self assert: (StringParser new parse: 'Jaime Lannister' with: 'Jaime') equals: (Accept new result: 'Jaime'; remainder: ' Lannister') .
	self assert: (StringParser new parse: '' with: 'Jaime Lannister') equals: (Reject new errorMessage: 'Not match requested STRING'; remainder: '') .
	self assert: (StringParser new parse: 'Jaime Lannister' with: 'Chayme Lannister') equals: (Reject new errorMessage: 'Not match requested STRING'; remainder: 'Jaime Lannister') .
	self assert: (StringParser new parse: 'Jaime Lannister' with: 'Jaime Lanister') equals: (Reject new errorMessage: 'Not match requested STRING'; remainder: 'Jaime Lannister') .
	self assert: (StringParser new parse: '' with: '') equals: (Reject new errorMessage: 'Not match requested STRING'; remainder: '') .
	self assert: (StringParser new parse: 'Jaime Lannister' with: '') equals: (Reject new errorMessage: 'Not match requested STRING'; remainder: 'Jaime Lannister') .