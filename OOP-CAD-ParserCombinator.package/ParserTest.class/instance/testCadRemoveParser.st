tests
testCadRemoveParser
	self assert: (CadRemoveParser new parse: 'remove 8,6') equals: (Accept new result: (CadRemoveCommand new point: (CadAbsolutePoint new point: 8@6)); remainder: '').
	self assert: (CadRemoveParser new parse: 'remove +8,-2') equals: (Accept new result: (CadRemoveCommand new point: (CadRelativePoint new point: 8@(-2))); remainder: '').
	self assert: (CadRemoveParser new parse: 'remove') equals: (Reject new errorMessage: 'Not match REMOVE'; remainder: 'remove').
	self assert: (CadRemoveParser new parse: ' ') equals: (Reject new errorMessage: 'Not match REMOVE'; remainder: ' ').
	self assert: (CadRemoveParser new parse: '') equals: (Reject new errorMessage: 'Not match REMOVE'; remainder: '').