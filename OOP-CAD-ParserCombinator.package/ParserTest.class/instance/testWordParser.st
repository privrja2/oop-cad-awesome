tests
testWordParser
	self assert: (WordParser new parse: 'x') equals: (Accept new result: 'x'; remainder: '').
	self assert: (WordParser new parse: ' xcad') equals: (Reject new errorMessage: 'Not match WORD'; remainder: ' xcad').
	self assert: (WordParser new parse: 'xcad ') equals: (Reject new errorMessage: 'Not match WORD'; remainder: 'xcad ').
	self assert: (WordParser new parse: 'x.cad') equals: (Accept new result: 'x.cad'; remainder: '').
	self assert: (WordParser new parse: 'save x.cad') equals: (Reject new errorMessage: 'Not match WORD'; remainder: 'save x.cad').
	self assert: (WordParser new parse: '') equals: (Reject new errorMessage: 'Text is empty'; remainder: '').