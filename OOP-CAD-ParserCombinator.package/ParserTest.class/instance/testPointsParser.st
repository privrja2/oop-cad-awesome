tests
testPointsParser
	self assert: (PointsParser new parse: '8,4') equals: (Accept new result: { CadAbsolutePoint new point: 8@4} asOrderedCollection; remainder: '').
	self assert: (PointsParser new parse: '+15,+25 18,24') equals: (Accept new result: { CadRelativePoint new point: 15@25 . CadAbsolutePoint new point: 18@24} asOrderedCollection; remainder: '').
	self assert: (PointsParser new parse: '+15,+25 18,24 -8,+9 5,5 -8,-9') equals: (Accept new result: { CadRelativePoint new point: 15@25 . CadAbsolutePoint new point: 18@24 . CadRelativePoint new point: -8@9 . CadAbsolutePoint new point: 5@5 . CadRelativePoint new point: -8@(-9)} asOrderedCollection; remainder: '').
	self assert: (PointsParser new parse: '8,-8, 18,24') equals: (Reject new errorMessage: 'Not match POINTS'; remainder: '8,-8, 18,24').
	self assert: (PointsParser new parse: '8,-8 18,24') equals: (Reject new errorMessage: 'Not match POINTS'; remainder: '8,-8 18,24').
	self assert: (PointsParser new parse: '8,8 18,24 -5,9') equals: (Reject new errorMessage: 'Not match POINTS'; remainder: '8,8 18,24 -5,9').
	self assert: (PointsParser new parse: 'xcad ') equals: (Reject new errorMessage: 'Not match POINTS'; remainder: 'xcad ').
	self assert: (PointsParser new parse: 'x.cad') equals: (Reject new errorMessage: 'Not match POINTS'; remainder: 'x.cad').
	self assert: (PointsParser new parse: ' ') equals: (Reject new errorMessage: 'Not match POINTS'; remainder: ' ').
	self assert: (PointsParser new parse: '') equals: (Reject new errorMessage: 'Not match POINTS'; remainder: '').