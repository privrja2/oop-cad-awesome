tests
testPointParser
	self assert: (PointParser new parse: '+15,+25') equals: (Accept new result: (CadRelativePoint new point: 15@25); remainder: '').
	self assert: (PointParser new parse: '+1,-18') equals: (Accept new result: (CadRelativePoint new point: 1@(-18)); remainder: '').
	self assert: (PointParser new parse: '15,25') equals: (Accept new result: (CadAbsolutePoint new point: 15@25); remainder: '').
	self assert: (PointParser new parse: '15,0') equals: (Accept new result: (CadAbsolutePoint new point: 15@0); remainder: '').
	self assert: (PointParser new parse: '15,') equals: (Reject new errorMessage: 'Not match POINT'; remainder: '15,').
	self assert: (PointParser new parse: 's15') equals: (Reject new errorMessage: 'Not match POINT'; remainder: 's15').