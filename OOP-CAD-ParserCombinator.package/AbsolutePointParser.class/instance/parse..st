accessing
parse: aText
	| x comma y natParser |
	natParser := NatParser new .
	x := natParser parse: aText .
	x isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectAbsolutePoint: aText ] .
	comma := CharParser new parse: x remainder with: $, .
	comma isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectAbsolutePoint: aText ] .
	y := natParser parse: comma remainder .
	y isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectAbsolutePoint: aText ] .
	^ Accept new result: (CadAbsolutePoint new point: (x result @ y result)); remainder: y remainder .
 