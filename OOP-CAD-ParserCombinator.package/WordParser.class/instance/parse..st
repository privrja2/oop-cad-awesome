accessing
parse: aText
	aText isEmpty
	ifTrue: [ ^ ParseResultFactory buildRejectTextEmptyFrom: aText ] .
	
	(aText matchesRegex: '^(\S)*$')
		ifTrue: [ ^ Accept new result: aText; remainder: '' ]
		ifFalse: [ ^ ParseResultFactory buildRejectWord: aText ]