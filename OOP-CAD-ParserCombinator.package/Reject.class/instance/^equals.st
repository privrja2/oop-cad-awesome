comparing
= aReject
	(aReject isMemberOf: Reject)
	ifTrue: [ ^ (self errorMessage = aReject errorMessage ) and: [ (self remainder = aReject remainder ) ] ]
	ifFalse: [ ^ false ]