accessing
parse: aText
	| x comma y intParser |
	intParser := IntParser new .
	x := intParser parse: aText .
	x isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectRelativePoint: aText ] .
	comma := CharParser new parse: x remainder with: $, .
	comma isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectRelativePoint: aText ] .
	y := intParser parse: comma remainder .
	y isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectRelativePoint: aText ] .
	^ Accept new result: (CadRelativePoint new point: (x result @ y result)); remainder: y remainder .