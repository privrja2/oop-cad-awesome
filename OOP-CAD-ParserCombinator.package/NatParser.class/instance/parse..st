accessing
parse: aText 
	| firstNum nextNum num nextNumParser |
	nextNumParser := NextNumParser new .
	firstNum := (FirstNumParser new parse: aText) .
	firstNum isAccepted
		ifFalse: [ 
			| zero |
			zero := StringParser new parse: aText with: '0' . 
			zero isAccepted
				ifFalse: [ ^ ParseResultFactory buildRejectNat: aText ] .
			nextNum := nextNumParser parse: zero remainder .
			nextNum isAccepted
				ifFalse: [ ^ Accept new result: 0; remainder: zero remainder ] .
			^ ParseResultFactory buildRejectNat: aText
			] .
	
	num := firstNum result asString .
	nextNum := nextNumParser parse: firstNum remainder .
	[ nextNum isAccepted ] whileTrue: [ 
		num := num , nextNum result asString .
		nextNum := nextNumParser parse: nextNum remainder .
	] .
	^ Accept new result: num asNumber; remainder: nextNum remainder .