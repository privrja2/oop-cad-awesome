build
buildAcceptCircleFromPoint: aSourcePoint radius: aRadius reminder: aReminder
	^ Accept new result: (CadCircleCommand new shape: (CadCircle fromCenterPoint: aSourcePoint radius: aRadius)); remainder: aReminder