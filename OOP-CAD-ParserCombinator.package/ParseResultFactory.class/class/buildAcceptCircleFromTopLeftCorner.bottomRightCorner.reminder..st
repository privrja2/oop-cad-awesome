build
buildAcceptCircleFromTopLeftCorner: aSourcePoint bottomRightCorner: aDestinationPoint reminder: aReminder
	^ Accept new result: (CadCircleCommand new shape: (CadCircle fromTopLeftCorner: aSourcePoint bottomRightCorner: aDestinationPoint)); remainder: aReminder