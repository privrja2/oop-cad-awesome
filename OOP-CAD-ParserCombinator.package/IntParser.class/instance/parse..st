accessing
parse: aText 
	| sign nat int |
	sign := SignParser new parse: aText .
	sign isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectInt: aText ] .
	
	int := sign result .
	nat := NatParser new parse: sign remainder .	
	nat isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectInt: aText ] .
		
	sign result = $+
		ifTrue: [ ^ nat ] .
	^ Accept new result: ('-', nat result asString) asNumber; remainder: nat remainder .