accessing
parse: aText
	| load name |
	load := StringParser new parse: aText with: 'load ' . 
	load isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectLoad: aText ] .
	name := WordParser new parse: load remainder .
	name isAccepted
		ifTrue: [ ^ Accept new result: (CadLoadCommand new name: name result); remainder: name remainder ] .
	^ ParseResultFactory buildRejectLoad: aText