tests
testCadCircle
	self assert: (CadCircle fromCenterPoint: (CadAbsolutePoint new point: 5@10) radius: 5) equals: (CadCircle fromTopLeftCorner: (CadAbsolutePoint new point: 0@5) width: 10 height: 10).
	self assert: (CadCircle fromCenterPoint: (CadAbsolutePoint new point: 5@10) radius: 5) equals: (CadCircle fromTopLeftCorner: (CadAbsolutePoint new point: 0@5) bottomRightCorner: (CadAbsolutePoint new point: 10@15)).
	self assert: (CadCircle fromTopLeftCorner: (CadAbsolutePoint new point: 0@5) width: 10 height: 10) equals: (CadCircle fromTopLeftCorner: (CadAbsolutePoint new point: 0@5) bottomRightCorner: (CadAbsolutePoint new point: 10@15)).
	self should: (CadCircle fromTopLeftCorner: (CadAbsolutePoint new point: 0@0) width: 5 height: 8) radius raise: IllegalState.
	self assert: (CadCircle fromTopLeftCorner: (CadAbsolutePoint new point: 0@0) width: 7 height: 7) radius equals: (7/2).
	
	
	
	