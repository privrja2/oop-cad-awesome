tests
testCadRectangle
	self assert: (CadRectangle fromTopLeftCorner: (CadAbsolutePoint new point: 5@5) bottomRightCorner: (CadAbsolutePoint new point: 8@7)) equals: (CadRectangle fromTopLeftCorner: (CadAbsolutePoint new point: 5@5) width: 3 height: 2).
	self assert: (CadRectangle fromTopLeftCorner: (CadAbsolutePoint new point: 5@0) bottomRightCorner: (CadAbsolutePoint new point: 8@7)) equals: (CadRectangle fromTopLeftCorner: (CadAbsolutePoint new point: 5@0) width: 3 height: 7).
	self assert: (CadRectangle fromTopLeftCorner: (CadAbsolutePoint new point: 5@5) bottomRightCorner: (CadAbsolutePoint new point: 8@7)) bottomRightCorner equals: (CadRectangle fromTopLeftCorner: (CadAbsolutePoint new point: 5@5) width: 3 height: 2) bottomRightCorner.
	