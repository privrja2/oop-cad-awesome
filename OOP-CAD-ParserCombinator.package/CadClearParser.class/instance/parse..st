accessing
parse: aText
	| clear |
	clear := StringParser new parse: aText with: 'clear' .
	clear isAccepted
		ifTrue: [ ^ Accept new result: CadClearCommand new; remainder: clear remainder ] .
	^ ParseResultFactory buildRejectClear: aText