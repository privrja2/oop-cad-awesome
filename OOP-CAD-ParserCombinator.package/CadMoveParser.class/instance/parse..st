accessing
parse: aText
	| move pointParser point space newPoint |
	move := StringParser new parse: aText with: 'move ' .
	move isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectMove: aText ] .
	
	pointParser := PointParser new .
	point := pointParser parse: move remainder .
	point isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectMove: aText ] .
		
	space := SpaceParser new parse: point remainder .
	space isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectMove: aText ] .
	
	newPoint := pointParser parse: space remainder .
	newPoint isAccepted
		ifTrue: [ ^ Accept new result: (CadMoveCommand new point: point result; newPoint: newPoint result); remainder: newPoint remainder ] .
	^ ParseResultFactory buildRejectMove: aText