accessing
parse: aText
	| save name |
	save := StringParser new parse: aText with: 'save ' . 
	save isAccepted
		ifFalse: [ ^ ParseResultFactory buildRejectSave: aText ] .
	name := WordParser new parse: save remainder .
	name isAccepted
		ifTrue: [ ^ Accept new result: (CadSaveCommand new name: name result); remainder: name remainder ] .
	^ ParseResultFactory buildRejectSave: aText