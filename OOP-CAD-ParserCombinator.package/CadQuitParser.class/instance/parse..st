accessing
parse: aText
	| quit |
	quit := StringParser new parse: aText with: 'quit' .
	quit isAccepted
		ifTrue: [ ^ Accept new result: CadQuitCommand new; remainder: quit remainder ] .
	^ ParseResultFactory buildRejectQuit: aText