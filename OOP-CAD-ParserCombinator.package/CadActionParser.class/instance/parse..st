accessing
parse: aText
	| text |
	text := aText trim .
	actions do: [ :action |
		| result |
		result := action new parse: text .
		(result isAccepted and: [ result remainder = '' ])
			ifTrue: [ ^ result ]
	] .
	^ ParseResultFactory buildRejectAction: aText