adding
whenElementsChangedDo: aBlock
	^ announcer when: CadElementsChangedAnnouncement do: aBlock.