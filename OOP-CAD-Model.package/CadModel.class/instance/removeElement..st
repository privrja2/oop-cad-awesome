removing
removeElement: aShape
	elementsShapes removeAt: (elementsShapes indexOf: aShape).
	announcer announce: CadElementsChangedAnnouncement.