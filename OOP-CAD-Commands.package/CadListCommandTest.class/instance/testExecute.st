tests
testExecute
	|cmd view context expectedOutput shapes shapesOnPoint shapesOutOfPoint|
	view := CadSavedOutputTestView new.
	context := CadController new view: view.
	cmd := CadListCommand new context: context.
	expectedOutput := String new.
	
	cmd execute.
	self assert: view output equals: expectedOutput.
	
	shapes := TestShapeBuilder buildShapes.
	shapes do: [ :shape | expectedOutput := expectedOutput, shape asString, Character cr asString. ].
	context model elementsShapes: shapes.
	cmd execute.
	self assert: view output equals: expectedOutput.
	
	cmd := CadListCommand new context: context; point: (CadAbsolutePoint new point: 10@10).
	context model emptyElementsShapes.
	view clearSavedOutput.
	expectedOutput := String new.
	shapesOnPoint := TestShapeBuilder buildShapesOnPoint10at10.
	shapesOutOfPoint := TestShapeBuilder buildShapesNotOnPoint10at10.
	shapesOnPoint do: [ :shape | expectedOutput := expectedOutput, shape asString, Character cr asString. ].
	context model elementsShapes: shapesOnPoint, shapesOutOfPoint.
	cmd execute.
	self assert: view output equals: expectedOutput.