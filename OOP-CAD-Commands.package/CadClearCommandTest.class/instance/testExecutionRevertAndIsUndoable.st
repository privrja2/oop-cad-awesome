tests
testExecutionRevertAndIsUndoable
	|cmd context shapes|
	context := CadController new.
	cmd := CadClearCommand new context: context.

	self assert: context model elementsShapes isEmpty.
	cmd execute.
	self assert: context model elementsShapes isEmpty.
	self assert: cmd isUndoable not.
	
	1 to: 10 do: [ :x |
		shapes := TestShapeBuilder buildShapes.
		cmd := CadClearCommand new context: context.
		context model elementsShapes: shapes.
		cmd execute.
		self assert: cmd isUndoable.
		self assert: context model elementsShapes isEmpty.
		cmd revert.
		self assert: context model elementsShapes equals: shapes.
	]