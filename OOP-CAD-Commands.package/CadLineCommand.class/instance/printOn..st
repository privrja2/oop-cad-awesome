printing
printOn: aStream
	aStream << 'line ' << shape startPoint asCmdString.
	shape points do: [ :point | aStream << ' ' << point asCmdString ].
	^aStream