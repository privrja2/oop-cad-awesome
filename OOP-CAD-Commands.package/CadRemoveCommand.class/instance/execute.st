accessing
execute
	oldState := context model elementsShapes copy.
	context model elementsShapes removeAllSuchThat: [ :shape | shape isIntercepting: point point ].
	context view update.