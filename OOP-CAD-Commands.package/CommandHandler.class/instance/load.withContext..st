do/undo
load: aFileName withContext: aContext
|aStream|
aStream:=(StandardFileStream 
 oldFileNamed: aFileName asString).  
aStream = StandardFileStream 
	ifFalse: [ 
		[aStream atEnd] whileFalse: [ | parseResult |
				parseResult := CadActionParser new parse: aStream nextLine. 
		 		parseResult isAccepted
					ifTrue: [ aContext commandHandler add: (parseResult result context: aContext). ]]]
	