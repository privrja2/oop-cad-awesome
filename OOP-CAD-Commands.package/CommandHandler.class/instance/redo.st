do/undo
redo
	position = commands size
		ifTrue: [ ^ self ] .
	position := position + 1 .
	(commands at: position) execute