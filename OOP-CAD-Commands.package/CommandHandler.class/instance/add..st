adding
add: aCommand
	commands := (commands removeFirst: position) asOrderedCollection .
	aCommand execute.
	aCommand isUndoable
		ifTrue: [ commands add: aCommand. ].
	position := commands size .