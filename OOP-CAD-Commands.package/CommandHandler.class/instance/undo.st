do/undo
undo
	position = 0
		ifTrue: [ ^ self ] .
	(commands at: position) revert .
	position := position - 1