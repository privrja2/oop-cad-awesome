do/undo
save: aFileName
|aString|
aString:=''.
(commands do: [:each | each class = CadSaveCommand ifFalse:[ aString:= aString , each printString, Character cr asString]]).
StandardFileStream
  fileNamed: aFileName
  do: [:str |	str
                nextPutAll: aString; 
                cr].