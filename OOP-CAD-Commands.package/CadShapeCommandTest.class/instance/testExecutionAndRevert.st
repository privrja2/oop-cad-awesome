tests
testExecutionAndRevert	
	|cmd context shape allShapes shapesToAdd|
	context := CadController new.
	shape := TestShapeBuilder buildShape.
	cmd := CadRectangleCommand new context: context ;shape: shape.
	allShapes := OrderedCollection with: shape.

	self assert: context model elementsShapes isEmpty.
	cmd execute.
	self assert: context model elementsShapes equals: allShapes.
	cmd revert.
	self assert: context model elementsShapes isEmpty.
	
	cmd execute.
	self assert: context model elementsShapes equals: allShapes.
	cmd revert.
	self assert: context model elementsShapes isEmpty.

1 to: 50 do: [ :x | 
	shape := TestShapeBuilder buildShape.
	cmd := CadRectangleCommand new context: context ;shape: shape.
	allShapes := OrderedCollection new.
	shapesToAdd := TestShapeBuilder buildShapes.
	allShapes addAll: shapesToAdd.
	allShapes add: shape.
	
	context model elementsShapes: shapesToAdd.
	
	self assert: context model elementsShapes equals: shapesToAdd.
	cmd execute.
	self assert: context model elementsShapes equals: allShapes.
	cmd execute.
	allShapes add: shape.
	self assert: context model elementsShapes equals: allShapes.
	cmd execute.
	allShapes add: shape.
	self assert: context model elementsShapes equals: allShapes.
	cmd revert.
	cmd revert.
	cmd revert.
	self assert: context model elementsShapes equals: shapesToAdd.
]