accessing
execute
	context model elementsShapes do: [ :shape |
		(self isPointSet not or: [(shape isIntercepting: self point point)] )
		ifTrue: [ context view writeOutput: shape asString ] ].