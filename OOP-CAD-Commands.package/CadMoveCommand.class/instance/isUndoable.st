testing
isUndoable
	^context model elementsShapes anySatisfy: [ :shape |
		shape isIntercepting: (newPoint asPoint: point). ]