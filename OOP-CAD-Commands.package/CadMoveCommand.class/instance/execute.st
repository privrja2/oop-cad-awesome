accessing
execute
	|shiftPoint|
	shiftPoint := (newPoint asPoint: point) - point point.
	context model elementsShapes:
		(context model elementsShapes
			collect: [ :shape |
				(shape isIntercepting: point point )
					ifTrue: [ shape shiftBy: shiftPoint ].
				shape yourself.
			]
		)