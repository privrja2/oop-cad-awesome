tests
testCommand
	|cmd context shapesOnPoint shapesOutOfPoint|
	context := CadController new.
	cmd := CadRemoveCommand new context: context; point: (CadAbsolutePoint new point: 10@10).
	
	self assert: context model elementsShapes isEmpty.
	cmd execute.
	self assert: context model elementsShapes isEmpty.
	self assert: cmd isUndoable not.
	
	shapesOnPoint := TestShapeBuilder buildShapesOnPoint10at10.
	shapesOutOfPoint := TestShapeBuilder buildShapesNotOnPoint10at10.
	context model elementsShapes: shapesOnPoint, shapesOutOfPoint.
	
	cmd execute.
	self assert: cmd isUndoable.
	self assert: context model elementsShapes equals: shapesOutOfPoint.
	cmd revert.
	self assert: context model elementsShapes equals: shapesOnPoint, shapesOutOfPoint.
	