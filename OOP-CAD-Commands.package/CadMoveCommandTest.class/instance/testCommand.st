tests
testCommand
	|cmd context expectedShapes circle|
	context := CadController new.
	cmd := CadMoveCommand new context: context; point: (CadAbsolutePoint new point: 10@10); newPoint: (CadAbsolutePoint new point: 40@40).

	cmd execute.
	self assert: cmd isUndoable not.

	cmd := CadMoveCommand new context: context; point: (CadAbsolutePoint new point: 10@10); newPoint: (CadAbsolutePoint new point: 40@40).
	circle := (CadCircle new centerPoint: (CadAbsolutePoint new point: 10@10) radius: 10 ).
	expectedShapes := OrderedCollection with: circle
		with: (CadCircle new centerPoint: (CadAbsolutePoint new point: 50@50) radius: 10 ). 

	context model elementsShapes: expectedShapes deepCopy.

	self assert: context model elementsShapes equals: expectedShapes.
	cmd execute.	
	self assert: cmd isUndoable.
	circle centerPoint: (CadAbsolutePoint new point: 40@40) radius: 10. 	
	self assert: context model elementsShapes equals: expectedShapes.
	cmd revert.
	circle centerPoint: (CadAbsolutePoint new point: 10@10) radius: 10. 	
	self assert: context model elementsShapes equals: expectedShapes.
